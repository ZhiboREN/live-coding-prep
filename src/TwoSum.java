import java.util.HashMap;
import java.util.Map;

public class TwoSum {

/*  Given an array of integers, return indices of the two numbers such that they add up to a specific target.

    You may assume that each input would have exactly one solution, and you may not use the same element twice.

    Example:

    Given nums = [2, 7, 11, 15], target = 9,

    Because nums[0] + nums[1] = 2 + 7 = 9,
    return [0, 1].*/

	public static void main(String[] args) {
		int[] array = new int[]{2, 5, 5, 11};
		int target = 10;
		for (int i : twoSum(array, target)) {
			System.out.println(i);
		}
		for (int i : bestSolution(array, target)) {
			System.out.println(i);
		}
	}

	// My answer
	private static int[] twoSum(int[] nums, int target) {
		for (int i = 0; i < nums.length - 1; i++) {
			// Note: for loop increment, int j = i+1
			for (int j = i + 1; j < nums.length; j++) {
				if (nums[i] + nums[j] == target) {
					return new int[]{i, j};
				}
			}
		}
		throw new IllegalArgumentException("No two sum solution");
	}

/*
	Reference answer
		One-pass Hash Table
		It turns out we can do it in one-pass. While we iterate and inserting elements into the table,
		we also look back to check if current element's complement already exists in the table. If it exists,
		we have found a solution and return immediately.

	Time complexity : O(n). We traverse the list containing n elements only once. Each look up in the table costs only O(1) time.
	Space complexity : O(n). The extra space required depends on the number of items stored in the hash table, which stores at most n elements.
*/

	private static int[] bestSolution(int[] nums, int target) {
		Map<Integer, Integer> map = new HashMap<>(); // ATTENTION!
		for (int i = 0; i < nums.length; i++) {
			int complement = target - nums[i];
			if (map.containsKey(complement)) {
				return new int[]{map.get(complement), i};
			}
			map.put(nums[i], i);
		}
		throw new IllegalArgumentException("No two sum solution");
	}
}
