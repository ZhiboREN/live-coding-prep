import java.util.ArrayList;

public class AddTwoNumbers {

/*	You are given two non-empty linked lists representing two non-negative integers.
	The digits are stored in reverse order and each of their nodes contain a single digit.
	Add the two numbers and return it as a linked list.

	You may assume the two numbers do not contain any leading zero, except the number 0 itself.

	Example:

	Input: (2 -> 4 -> 4) + (5 -> 6 -> 4)
	Output: 7 -> 0 -> 9
	Explanation: 442 + 465 = 917.*/

	public static void main(String[] args) {
		ListNode n1 = new ListNode(2);
		ListNode n2 = new ListNode(4);
		ListNode n3 = new ListNode(4);
		n1.next = n2;
		n2.next = n3;

		ListNode n4 = new ListNode(5);
		ListNode n5 = new ListNode(6);
		ListNode n6 = new ListNode(4);
		n4.next = n5;
		n5.next = n6;

		ListNode ret = addTwoNumbers(n1, n4);
		while (ret != null) {
			System.out.println(ret.val);
			ret = ret.next;
		}
	}

	private static ListNode addTwoNumbers(ListNode l1, ListNode l2) {
		int carry = 0;
		ArrayList<ListNode> answer = new ArrayList<>(); // ATTENTION!
		while (!(l1 == null && l2 == null)) { // ATTENTION!
			ListNode ln = new ListNode(0);
			if ((l1 == null ? 0 : l1.val) + (l2 == null ? 0 : l2.val) + carry >= 10) { // ATTENTION!
				ln.val = (l1 == null ? 0 : l1.val) + (l2 == null ? 0 : l2.val) + carry - 10;
				carry = 1;
			} else {
				ln.val = (l1 == null ? 0 : l1.val) + (l2 == null ? 0 : l2.val) + carry;
				carry = 0;
			}
			answer.add(ln);
			if (l1 != null) {
				l1 = l1.next;
			}
			if (l2 != null) {
				l2 = l2.next;
			}
		}
		if (carry == 1) {
			answer.add(new ListNode(1));
		}
		for (int i = 1; i < answer.size(); i++) { // ATTENTION!
			answer.get(i - 1).next = answer.get(i);
		}
		return answer.get(0);
	}
}

//	  Definition for singly-linked list.
class ListNode {
	int val;
	ListNode next;

	ListNode(int x) {
		val = x;
	}
}

